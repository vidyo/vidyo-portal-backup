# Vidyo Portal Backup

This application makes a backup of the Vidyo Portal using its API.

## Installation

1. Install the dependencies
    1. ```pip install -r requirements.tx```
1. Make sure that the conf.py file exists and has the correct values.
1. Run the application
    1. ```python vidyo_backup.py```
    


