import glob
import os
import dateutil.relativedelta
import pytz

import datetime
import requests
from api.vidyo.VidyoSuperAPI import VidyoAPISuper
from common.logger import setup_logging
from conf import VIDYO_SUPER_API_USERNAME, VIDYO_SUPER_API_PASSWORD, BACKUPS_FOLDER, DELETE_BACKUPS_OLDER_THAN

logging = setup_logging()
logger = logging.getLogger(__name__)


def get_vidyo_backup_list():
    """
    Gets a list of all the available backups on the server
    :return: the backups list
    """
    logger.debug("Getting backups...")
    backups_list = VidyoAPISuper.list_db()
    return backups_list


def download_vidyo_backup(backup):
    """
    Downloads a Vidyo Portal backup to a selected folder,
    :param backup: the backup to download
    :return:
    """
    logger.info("download_vidyo_backup")
    local_filename = BACKUPS_FOLDER + backup.backupURL.split('/')[-1]
    resp = requests.get(backup.backupURL, auth=(VIDYO_SUPER_API_USERNAME, VIDYO_SUPER_API_PASSWORD), stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in resp.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
        logger.info("Backup %s saved." % local_filename)


def download_latest_vidyo_backup(backups_list):
    """
    Downloads the latest backup from a list
    :param backups_list: List of backups
    :return:
    """
    if len(backups_list)>0:
        download_vidyo_backup(backups_list[0])


def download_vidyo_backups(backups_list):
    """
    Downloads a list of Vidyo Portal backups
    :param backups_list: Backups to download
    :return:
    """
    logger.debug("Downloading backups...")
    for backup in backups_list:
        download_vidyo_backup(backup)


def save_vidyo_backup(encrypt_password):
    """
    Creates a new Vidyo Portal backup. This function can take a bit on executing, befare of the timeouts.
    :param encrypt_password: Password to encrypt the backup
    :return: The recently created backup
    """
    logger.debug("Saving a new backup...")
    backup = VidyoAPISuper.backup_db(encrypt_password)
    return backup


def delete_vidyo_backup(backup_name):
    """
    Deletes an existing backup from the Vidyo Portal
    :param backup_name:
    :return:
    """
    logger.debug("Delete an existing backup...")
    backup = VidyoAPISuper.delete_db(backup_name)
    return backup


def delete_old_backups_locally():
    """
    Deletes all the backups older than DELETE_BACKUPS_OLDER_THAN months
    :return:
    """
    logger.info("delete_old_backups_locally")
    existing_backups = glob.glob(BACKUPS_FOLDER + "*.veb")

    logger.debug(existing_backups)

    today = datetime.datetime.now()
    delete_end_date = today - dateutil.relativedelta.relativedelta(months=DELETE_BACKUPS_OLDER_THAN)

    for backup in existing_backups:
        created_date = datetime.datetime.utcfromtimestamp(os.path.getctime(backup))
        logger.debug(created_date)
        if created_date < delete_end_date:
            logger.debug("%s Will be deleted", backup)
            os.remove(backup)


def delete_old_backups_vidyoportal():
    """
    Deletes backups stored on the Vidyo Portal older than DELETE_BACKUPS_OLDER_THAN months
    :return:
    """
    logger.info("delete_old_backups_vidyoportal")
    backups = get_vidyo_backup_list()
    mytz = pytz.timezone('Europe/Zurich')

    today = datetime.datetime.now()
    delete_end_date = today - dateutil.relativedelta.relativedelta(months=DELETE_BACKUPS_OLDER_THAN)
    delete_end_date = mytz.normalize(mytz.localize(delete_end_date, is_dst=True))

    for backup in backups:
        if backup.timestamp < delete_end_date:
            logger.debug("%s Will be deleted", backup.backupURL)
            file_to_delete = os.path.basename(backup.backupURL)
            logger.debug(delete_vidyo_backup(backup_name=file_to_delete))


if __name__ == "__main__":
    """
    ENTRY POINT
    """
    logger.info("main")
    saved_backup = save_vidyo_backup(VIDYO_SUPER_API_PASSWORD)
    download_vidyo_backup(saved_backup)
    delete_old_backups_locally()
    delete_old_backups_vidyoportal()



