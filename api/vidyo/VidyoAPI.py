from urllib2 import URLError
from suds import transport

from common.logger import setup_logging

logging = setup_logging()
logger = logging.getLogger(__name__)


class VidyoAPIClientBase(object):
    """
        Base class for AdminClient and UserClient
        Provides a method to obtain the correct transport for authentication.
    """

    @classmethod
    def get_transport(cls, url, username, password):
        """
            Note: we always use http.HttpAuthenticated, which implements basic authentication.
            https.HttpAuthenticated implements advanced authentication.
            Even if the 2 classes have "http" and "https" in the package names, they are just 2 ways
            of authenticating independant of http and https.
            Vidyo probably only supports basic authentication, be it through http or https.
        """
        return transport.https.HttpAuthenticated(username=username, password=password, timeout=30.0)


class VidyoAPIBase(object):
    """
        Base operations for Vidyo API clients.
    """

    @classmethod
    def get_vidyo_client(cls):
        pass

    @classmethod
    def _handle_service_call_exception(cls, service, e):
        cause = e.args[0]
        if type(cause) is tuple and cause[0] == 401:
            logger.exception("Exception in VidyoAPI service %s: %s" % (service, e))
        elif type(e) == URLError:
            logger.exception("Exception in VidyoAPI service %s: %s" % (service, e))
        else:
            raise

    @classmethod
    def _api_operation(cls, service, *params, **kwargs):
        try:
            vidyo_client = cls.get_vidyo_client()
        except Exception as e:
            raise Exception("could not connect to VidyoAPI: '%s'" % e)
        try:
            return getattr(vidyo_client.service, service)(*params, **kwargs)
        except Exception as e:
            cls._handle_service_call_exception(service, e)
