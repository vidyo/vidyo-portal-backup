from suds import client

import conf
from api.vidyo.VidyoAPI import VidyoAPIBase, VidyoAPIClientBase


class VidyoAPIAdminClient(VidyoAPIClientBase):
    """
        Singleton for the client for Vidyo's admin API.
        We need to build a suds.client.Client with a transport argument (and not directly with username and password
        arguments) because Vidyo only supports preemptive http authentication for performance reasons.
        Suds supports this by first constructing an HttpAuthenticated transport and then passing it to the Client.
    """
    _instance = None

    @classmethod
    def get_instance(cls, admin_api_url=None, username=None, password=None):
        if cls._instance is None or (admin_api_url is not None or username is not None or password is not None):

            if admin_api_url is None:
                admin_api_url = conf.VIDYO_ADMIN_API_WSDL_URL
            if username is None:
                username = conf.VIDYO_ADMIN_API_USERNAME
            if password is None:
                password = conf.VIDYO_ADMIN_API_PASSWORD

            location = conf.VIDYO_ADMIN_API_LOCATION

            try:
                cls._instance = client.Client(admin_api_url,
                                              transport=VidyoAPIClientBase.get_transport(admin_api_url, username,
                                                                                         password), location=location)
            except Exception as err:
                raise Exception(err)

        return cls._instance


class VidyoAPIAdmin(VidyoAPIBase):
    """
        This class performs low-level operations by getting the corresponding
        client and calling a SOAP service.
        Each class method performs a single service call to Vidyo.
    """

    @classmethod
    def get_vidyo_client(cls):
        return VidyoAPIAdminClient.get_instance()

    @classmethod
    def get_rooms(cls, filters=None):
        return cls._api_operation('getRooms', filters)

    @classmethod
    def get_participants(cls, filters=None):
        return cls._api_operation('getParticipants', filters)
