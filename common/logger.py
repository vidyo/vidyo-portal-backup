import logging
from conf import DEBUG, LOGS_FOLDER


def setup_logging():
    if DEBUG:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(filename=LOGS_FOLDER+'vidyo_backup.log', level=level)
    logger = logging.getLogger(__name__)
    logger.info('Logging Started')
    logger.debug('Logging Started')

    return logging
