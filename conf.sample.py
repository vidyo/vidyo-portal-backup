DEBUG = True
BACKUPS_FOLDER = 'backups/'
LOGS_FOLDER = 'logs/'
SOAP_CALL_TIMEOUT = 120
DELETE_BACKUPS_OLDER_THAN = 4  # MONTHS

#############################
# ADMIN API
#############################
VIDYO_ADMIN_API_WSDL_URL = ''
VIDYO_ADMIN_API_USERNAME = ''
VIDYO_ADMIN_API_PASSWORD = ''
VIDYO_ADMIN_API_LOCATION = ''

#############################
# SUPER API
#############################
VIDYO_SUPER_API_WSDL_URL = ''
VIDYO_SUPER_API_USERNAME = ''
VIDYO_SUPER_API_PASSWORD = ''
VIDYO_SUPER_API_LOCATION = ''
